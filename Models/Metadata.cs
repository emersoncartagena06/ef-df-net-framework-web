﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EjemploWebFrameworkEF.Models
{
  
    public class marcaMetadata
    {
        [Display(Name = "ID")]
        public int id_marca { get; set; }

        [Display(Name = "Marca")]
        [Required(ErrorMessage ="Debe ingresar el nombre de la marca")]
        public string marca1 { get; set; }

        [Display(Name = "País")]
        [Required(ErrorMessage = "Debe ingresar el país de la marca")]
        public string pais { get; set; }
    }

}