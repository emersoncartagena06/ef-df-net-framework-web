﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EjemploWebFrameworkEF.Models;

namespace EjemploWebFrameworkEF.Controllers
{
    public class viewModeloController : Controller
    {
        private autosEntities db = new autosEntities();

        // GET: viewModelo
        public ActionResult Index()
        {
            return View(db.vw_modelo.ToList());
        }

        // GET: viewModelo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vw_modelo vw_modelo = db.vw_modelo.Find(id);
            if (vw_modelo == null)
            {
                return HttpNotFound();
            }
            return View(vw_modelo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
